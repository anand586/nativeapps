import React from "react";
import { View, Text, Button, StyleSheet } from "react-native";

const CardView = props => {
    return <View style={{ ...styles.cardStyle, ...props.style }}>{props.children}</View>
};

const styles = StyleSheet.create({
    cardStyle: {
        shadowColor: "black",
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 5,
        shadowOpacity: 0.26,
        backgroundColor: "silver",
        padding: 30,
        borderRadius: 10
    }
});

export default CardView;