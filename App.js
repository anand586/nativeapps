import React, { useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';

import Header from "./components/Header";
import StartGameView from "./views/StartGameView";
import GameView from "./views/GameView";
import GameOverView from "./views/GameOverView";

export default function App() {
  const [userNumber, setUserNumber] = useState();
  const [guessRounds, setGuessRounds] = useState(0);

  const startGameHandler = selectNumber => {
    setUserNumber(selectNumber);
    setGuessRounds(0);
  };

  const gameOverHandler = numofRounds => {
    setGuessRounds(numofRounds);
  };

  let content = <StartGameView onStartGame={startGameHandler} />;

  if (userNumber && guessRounds <= 0) {
    content = <GameView userChoice={userNumber} onGameOver={gameOverHandler} />;
  }
  else if(guessRounds > 0){
content = <GameOverView/>;
  }

  return (
    <View style={styles.screen}>
      <Header title="GUESS A NUMBER"/>
      {content}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  screen: {
    flex: 1
  }
});
