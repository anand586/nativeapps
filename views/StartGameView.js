import React, { useState } from "react";
import { View, Text, StyleSheet, TextInput, Button, TouchableWithoutFeedback, Keyboard, Alert } from "react-native";

import CardView from "../components/CardView";
import Colors from "../constants/Colors";
import Input from "../components/Input";
import NumberContainer from "../components/NumberContainer";

const StartGameView = props => {

    const [value, SetValue] = useState("");
    const [confirm, setConfirm] = useState(false);
    const [selectNumber, setSelectnumber] = useState();


    const numberInputHnadler = inputText => {
        SetValue(inputText.replace(/[^0-9]/g, ""));
    };

    const resetInputandler = () => {
        SetValue("");
        setConfirm(false);
    };

    const confirmInputHandler = () => {
        const chosenNumber = parseInt(value);
        if (isNaN(chosenNumber) || chosenNumber <= 0 || chosenNumber > 99) {
            Alert.alert("Invalid Number", "Number has to be between 1 - 99", [{ text: "OKAY", style: "destructive", onPress: resetInputandler }]);
            return;
        }
        setConfirm(true);
        setSelectnumber(chosenNumber);
        SetValue("");
        Keyboard.dismiss();
    };

    let confirmedOutput;
    if (confirm) {
        confirmedOutput = (
            <CardView style={styles.summaryContainer}>
                <Text>You Selected</Text>
                <NumberContainer>{selectNumber}</NumberContainer>
                <Button title="START GAME" onPress={() => props.onStartGame(selectNumber)} />
            </CardView >
        );
    }

    return (
        <TouchableWithoutFeedback onPress={() => { Keyboard.dismiss(); }}>
            <View style={styles.screen}>
                <Text style={styles.title}>GAME BEGINS!</Text>
                <CardView style={styles.inputContainer}>
                    <Text>Select A Number</Text>
                    <Input style={styles.input} blurOnSubmit autoCapitalize="none" autoCorrect={false} keyboardType="number-pad" maxLength={2} onChangeText={numberInputHnadler} value={value} />
                    <View style={styles.buttonContainer}>
                        <View style={styles.button}><Button title="RESET" onPress={resetInputandler} color={Colors.accent} /></View>
                        <View style={styles.button}><Button title="CONFIRM" onPress={confirmInputHandler} color={Colors.primary} /></View>
                    </View>
                </CardView>
                {confirmedOutput}
            </View>
        </TouchableWithoutFeedback>
    );
};

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        padding: 10,
        alignItems: "center"
    },
    title: {
        fontSize: 20,
        marginVertical: 10
    },
    inputContainer: {
        width: 300,
        maxWidth: "80%",
        alignItems: "center"
    },
    buttonContainer: {
        flexDirection: "row",
        width: "100%",
        justifyContent: "space-between",
        paddingHorizontal: 15
    },
    button: {
        width: 100
    },
    input: {
        width: 50,
        textAlign: "center"
    },
    summaryContainer: {
        marginTop: 20,
        alignItems: "center"
    }
});

export default StartGameView;