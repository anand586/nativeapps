import React from "react";
import {View, Text, TextInput, Button, StyleSheet} from "react-native";

const GameOverView = props => {
    return(
        <View style={styles.screen}>
            <Text>Game OVER!!</Text>
        </View>
    );
};

const styles = StyleSheet.create({
screen:{
    flex:1,
    justifyContent:"center",
    alignItems:"center"
}
});

export default GameOverView;